"""
Translation unit tests
"""

import os
import unittest
import time

import systranio
from systranio.exceptions import ParameterError, ApiKeyError, ApiFailure
from systranio.Translation import (TextResult, LanguagePairResult,
                                   ProfileResult, FileResult, FileStatusResult,
                                   SupportedFormatResult)

API_KEY = os.environ['SYSTRANIO_KEY']  # required


class TestOtherTranslation(unittest.TestCase):
    """
    Test secondary Translation API calls, like api version, languages list, etc.
    """

    def setUp(self):
        self.translation = systranio.Translation(API_KEY)

    def test_invalid_api_key(self):
        "Failure with a missing api key"
        translation = systranio.Translation('')
        with self.assertRaises(ApiKeyError):
            translation.text('pas de clé !', 'en')

    def test_api_failure(self):
        "An invalid route (translation from en to en)"
        options = {'source': 'en'}
        with self.assertRaises(ApiFailure):
            self.translation.text('what ?', 'en', **options)

    def test_api_version(self):
        "api version number should be 1.0.0"
        version = self.translation.api_version()
        self.assertAlmostEqual(version, '1.0.0')

    def test_supported_languages_list(self):
        "supported_language returns a list"
        languages = self.translation.supported_languages()
        self.assertIsInstance(languages, list)

    def test_supported_languages_item(self):
        "supported_language items is a list of LanguagePairResult"
        languages = self.translation.supported_languages()
        self.assertIsInstance(languages[0], LanguagePairResult)

    def test_profiles_list(self):
        "profiles returns a list"
        profiles = self.translation.profiles()
        self.assertIsInstance(profiles, list)

    def test_profiles_item(self):
        "profiles items is  a list of LanguagePairResult"
        profiles = self.translation.profiles()
        self.assertIsInstance(profiles[0], ProfileResult)

    def test_supported_formats_list(self):
        "suported_formats returns a list"
        formats = self.translation.supported_formats()
        self.assertIsInstance(formats, list)

    def test_supported_formats_item(self):
        "supported_formats items is a list of SupportedFormatResult"
        formats = self.translation.supported_formats()
        self.assertIsInstance(formats[0], SupportedFormatResult)


class TestTextTranslation(unittest.TestCase):
    """
    Test Text Translation API
    """

    def setUp(self):
        self.translation = systranio.Translation(API_KEY)

    def test_simple_text_translation(self):
        "A simple translation must returns something"
        options = {'source': 'en'}
        result = self.translation.text('hello', 'fr', **options)
        self.assertIsInstance(result, TextResult)

    def test_multiple_text_translation(self):
        "A list of str to translate returns a list of TextResult"
        options = {'source': 'en'}
        results = self.translation.text(['hello', 'goodbye'], 'fr', **options)
        self.assertIsInstance(results, list)
        self.assertIsInstance(results[0], TextResult)

    def test_invalid_parameter(self):
        "Invalid parameter added to the translation query"
        options = {'jean_michel': 'margaret'}
        with self.assertRaises(ParameterError):
            self.translation.text('cats', 'en', **options)


class TestFileTranslation(unittest.TestCase):
    """
    Test File Translation API
    """

    def setUp(self):
        self.translation = systranio.Translation(API_KEY)
        self.input_file = 'README.md'

    def test_sync_file_translation(self):
        "A synchronous translation must returns a FileResult"
        options = {'source': 'en'}
        result = self.translation.file(self.input_file, 'fr', **options)
        self.assertIsInstance(result, FileResult)

    def test_async_file_translation(self):
        "An asynchronous translation must returns a request_id"
        options = {'source': 'en'}
        request_id = self.translation.file_async(self.input_file, 'fr',
                                                 **options)
        self.assertIsInstance(request_id, str)

    def test_status_file_translation(self):
        "Check the status of an asynchronous request"
        options = {'source': 'en'}
        request_id = self.translation.file_async(self.input_file, 'fr',
                                                 **options)
        status = self.translation.status(request_id)
        self.assertIsInstance(status, FileStatusResult)

    def test_cancel_file_translation(self):
        "Cancel an asynchronous request"
        options = {'source': 'en'}
        request_id = self.translation.file_async(self.input_file, 'fr',
                                                 **options)
        self.assertTrue(self.translation.cancel(request_id))

    def test_result_file_translation(self):
        "Result of an asynchronous request"
        options = {'source': 'en'}
        request_id = self.translation.file_async(self.input_file, 'fr',
                                                 **options)
        status = self.translation.status(request_id)
        retries = 1
        while status.success is not True:
            if retries > 5:
                self.fail('failure after 5 seconds timeout to get result')
            retries += 1
            time.sleep(1)
            status = self.translation.status(request_id)
        result = self.translation.result(request_id)
        self.assertIsInstance(result, str)


class TestFileResult(unittest.TestCase):
    """
    Test FileResult
    """

    def setUp(self):
        self.translation = systranio.Translation(API_KEY)
        self.input_file = 'README.md'

    def test_repr(self):
        "__repr__ should be output"
        options = {'source': 'en'}
        result = self.translation.file(self.input_file, 'fr', **options)
        self.assertEqual(str(result), result.output)

    def test_detected(self):
        "detected langages when source is not set"
        result = self.translation.file(self.input_file, 'fr')
        self.assertEqual(result.detected_language, 'en')
        self.assertTrue(0 <= result.detected_language_confidence <= 1)


class TestTextResult(unittest.TestCase):
    """
    Test TextResult
    """

    def setUp(self):
        self.api_response = {
            'output': 'diffamation',
            'stats': {
                'nb_characters': 10,
                'nb_tus_failed': 0,
                'nb_tus': 1,
                'nb_tokens': 1,
                'elapsed_time': 20
            }
        }

    def test_repr(self):
        "__repr__ should be output"
        result = TextResult(self.api_response)
        self.assertEqual(str(result), result.output)


class TestFileStatusResult(unittest.TestCase):
    """
    Test FileResult
    """

    def setUp(self):
        self.in_progress = {
            "batchId": "54a3d860e62ea467b136eddb",
            "cancelled": 'false',
            "createdAt": 1242234432,
            "description": "?",
            "expireAt": 1242234900,
            "finishedAt": 0,
            "finishedSteps": 3,
            "status": "registered",
            "totalSteps": 6
        }
        self.cancelled = {
            "batchId": "54a3d860e62ea467b136eddb",
            "cancelled": 'true',
            "createdAt": 1242234432,
            "description": "?",
            "expireAt": 1242234900,
            "finishedAt": 0,
            "finishedSteps": 3,
            "status": "registered",
            "totalSteps": 6
        }
        self.error = {
            "batchId": "54a3d860e62ea467b136eddb",
            "cancelled": 'false',
            "createdAt": 1242234432,
            "description": "?",
            "expireAt": 1242234900,
            "finishedAt": 0,
            "finishedSteps": 3,
            "status": "error",
            "totalSteps": 6
        }
        self.finished = {
            "batchId": "54a3d860e62ea467b136eddb",
            "cancelled": 'false',
            "createdAt": 1242234432,
            "description": "success",
            "expireAt": 1242234900,
            "finishedAt": 0,
            "finishedSteps": 3,
            "status": "finished",
            "totalSteps": 6
        }

    def test_in_progress(self):
        "success should be None"
        status = FileStatusResult(self.in_progress)
        self.assertIsNone(status.success)

    def test_fail(self):
        "success should be False if cancelled or in error"
        status = FileStatusResult(self.cancelled)
        self.assertFalse(status.success)
        status = FileStatusResult(self.error)
        self.assertFalse(status.success)

    def test_finished(self):
        "success should be True"
        status = FileStatusResult(self.finished)
        self.assertTrue(status.success)

    def test_str(self):
        "to string"
        status = FileStatusResult(self.finished)
        self.assertEqual(str(status), 'finished')


class TestLanguagePairResult(unittest.TestCase):
    """
    Test LanguagePairResult
    """

    def setUp(self):
        self.api_response = {
            "source": "nl",
            "target": "en",
            "profiles": [{
                "id": 0,
                "private": False
            }]
        }

    def test_str(self):
        "__repr__ should be output"
        result = LanguagePairResult(self.api_response)
        self.assertEqual(str(result), 'nl → en')


class TestProfilesResult(unittest.TestCase):
    """
    Test ProfileResult
    """

    def setUp(self):
        self.api_response = {"id": 0, "name": "TEST", "localization": {}}

    def test_str(self):
        "__repr__ should be output"
        result = ProfileResult(self.api_response)
        self.assertEqual(str(result), 'TEST')


class TestSupportedFormatResult(unittest.TestCase):
    """
    Test SupportedFormatResult
    """

    def setUp(self):
        self.api_response = {
            "mimetypes": {
                "input": "text/bitext",
                "output": "text/bitext"
            },
            "name": "bitext"
        }

    def test_str(self):
        "usable __str__ method"
        result = SupportedFormatResult(self.api_response)
        self.assertEqual(str(result), 'bitext : text/bitext → text/bitext')


if __name__ == '__main__':
    unittest.main()
