# Changelog


## Next version

### New

* Usr supported_formats call. [Renaud Canarduck]

### Fix

* Test detected_language. [Renaud Canarduck]


## v0.5.0 (2017-09-24)

### Fix

* Translation file result. [Renaud Canarduck]


## v0.4.1 (2017-09-17)

### New

* Cancel translation api call. [Renaud Canarduck]


## v0.4.0 (2017-09-17)

### New

* File status api. [Renaud Canarduck]


## v0.3.0 (2017-09-10)

### New

* Init file translation api. [Renaud Canarduck]

### Fix

* Functional sync file translation. [Renaud Canarduck]


## v0.2.10 (2017-09-05)

### New

* Text() accepts an array of strings. [Renaud Canarduck]


## v0.2.9 (2017-09-05)

### Changes

* Better dev docs. [Renaud Canarduck]


## v0.2.7 (2017-09-05)

### Changes

* Include coverage html report in docs. [Renaud Canarduck]

* Update logo & docs settings. [Renaud Canarduck]


## v0.2.2 (2017-09-04)

### Changes

* Updated Translation documentation. [Renaud Canarduck]

* Full documentation rewrite. [Renaud Canarduck]

* Switch from sphinx doc to mkdocs. [Renaud Canarduck]


## v0.1.4 (2017-09-03)

### New

* Profiles call. [Renaud Canarduck]


## v0.1.2 (2017-09-03)

### New

* Supported_languages call. [Renaud Canarduck]


## v0.1.1 (2017-09-02)

### New

* Api_version. [Renaud Canarduck]

### Fix

* Dev Gitlab CI. [Renaud Canarduck]


## v0.1.0 (2017-09-02)

### Other

* Dev: pkg: messing with bumpversion. [Renaud Canarduck]


