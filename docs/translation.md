# Translation

Translation API is a tool that automatically translates text from one language to another.
Language codes used are part of [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)

See [official Translation API Reference](https://platform.systran.net/reference/translation) for more infos.

## Text

Translate text from source language to target language.

```text(input, target, **options)```

### Required parameters

* `input` (str|list) : Input text, use a list for multiple translations at the same time
* `target` (str) : Target language code

### Available options :

* `source` = 'auto' (str) : Source language code (details) or auto. When the value is set to auto, the language will be automatically detected, used to enhance the translation, and returned in output.
* `format` = None (str) : Format of the source text. see [formats api response](#format).
* `profile` = None (str) : Profile id to apply for translation, see [profiles](#profiles).
* `with_source` = False (bool) : If true, the source will also be sent back in the response message. It can be useful when used with the withAnnotations option to align the source document with the translated document
* `with_annotations` = False (bool) : If true, different annotations will be provided in the translated document. If the option 'withSource' is used, the annotations will also be provided in the source document. It will provide segments, tokens, not found words,...
* `with_dictionary` = None (str) : User Dictionary or/and Normalization Dictionary ids to be applied to the translation result.
* `with_corpus` = None (str) : Corpus to be applied to the translation result.
* `back_translation` = False (bool) : If True, the translated text will be translated back in source language
* `options` = None (list) : Additional advanced options that may be given by SYSTRAN Support team for specific use cases.
* `encoding` = 'utf-8' (str) : Encoding. base64 can be useful to send binary documents in the JSON body.

### Example

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
options = {'source': 'en' }  # optional
result = translation.text('translation', 'fr', **options)
print(result)  # traduction
```

## File

Translate a file from source language to target language. To perform an asynchronous translation, use the `file_async` method and related api calls (status, result & cancel) 

```
file(input, target, **options)
file_async(input, target, **options)
```

### Required parameters

* `input` (file) : Input file
* `target` (str) : Target language code

### Available options :

* `source` = 'auto' (str) : Source language code (details) or auto. When the value is set to auto, the language will be automatically detected, used to enhance the translation, and returned in output.
* `format` = None (str) : Format of the source text. see [formats api response](#format).
* `profile` = None (str) : Profile id to apply for translation, see [profiles](#profiles).
* `with_source` = False (bool) : If true, the source will also be sent back in the response message. It can be useful when used with the withAnnotations option to align the source document with the translated document
* `with_annotations` = False (bool) : If true, different annotations will be provided in the translated document. If the option 'withSource' is used, the annotations will also be provided in the source document. It will provide segments, tokens, not found words,...
* `with_dictionary` = None (str) : User Dictionary or/and Normalization Dictionary ids to be applied to the translation result.
* `with_corpus` = None (str) : Corpus to be applied to the translation result.
* `back_translation` = False (bool) : If True, the translated text will be translated back in source language
* `options` = None (list) : Additional advanced options that may be given by SYSTRAN Support team for specific use cases.
* `encoding` = 'utf-8' (str) : Encoding. base64 can be useful to send binary documents in the JSON body.
* batch_id = None (str) : Identifier of the batch to which the translation request will be associated. Only asynchronous requests can be associated to a batch.

### Example

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
options = {'source': 'en' }  # optional
with open('translation.txt') as input_file:
    result = translation.file(input_file.read(), 'fr', **options)
print(result)  # traduction

options = {'source': 'en'}  
with open('translation.txt') as input_file:
    request_id = translation.file_async(input_file.read(), 'fr', **options)
print(translation.status(request_id))
result = translation.result(request_id)
print(result)  # traduction
```

## Translation Status

Get the status of an asynchronous file translation request. The translation result is available when the value of the status field is finished.
The translation request is unsuccessful when the value of the status field is error. 

```status(request_id)```

### Required parameters

* `request_id` (str) : Request identifier

### Example

```
options = {'source': 'en'}  
with open('translation.txt') as input_file:
    request_id = translation.file(input_file.read(), 'fr', True, **options)
status = translation.status(request_id)
print(status.success)  # True if finished, False if error, None if in progress
```

## Translation Cancel

Cancel an asynchronous translation request. Requests with status Finished or error are not modified. Requests with other statuses are cancelled. Note that for the statuses started and pending, the cancellation can take some time to be effective. 

```cancel(request_id)```

### Required parameters

* `request_id` (str) : Request identifier

### Example

```
options = {'source': 'en'}  
with open('translation.txt') as input_file:
    request_id = translation.file(input_file.read(), 'fr', True, **options)
status = translation.status(request_id)
print(status.success)  # None
translation.cancel(request_id)
status = translation.status(request_id)
print(status.success)  # False
```

## Translation Result

Get the result of an asynchronous translation request

```result(request_id)```

### Required parameters

* `request_id` (str) : Request identifier

### Example

```
options = {'source': 'en'}  
with open('translation.txt') as input_file:
    request_id = translation.file(input_file.read(), 'fr', True, **options)
result = translation.result(request_id)
print(result)  # traduction
```

## Translation API version

Current version for translation apis.

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
result = translation.api_version()
print(result)  # 1.0.0
```

## Translation supported languages

List of language pairs in which translation is supported. This list can be limited to a specific source language or target language.

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
source = fr # optional
result = translation.supported_languages(source)
for language_pair in result:
    print(language_pair)  # fr -> en, fr -> de, etc.
```

## Translation profiles

List all available profiles for translation.

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
result = translation.profiles()
for profile in result:
    print(profile)  # Language Identification, Segmentation TH Generic, etc.
```

## Supported formats

List of supported formats with their outputs for the translation.

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
result = translation.supported_formats()
for format in result:
    print(format)  # html (text/html -> text/html)
```

## Exceptions

3 custom exceptions are thrown in case of failure.

* `ParameterError`: invalid parameter send to an api call
* `ApiKeyError`: problem with the current api key (error 400)
* `ApiFailure`: sytran.io encountered an error, often happens when source language (sometimes auto-detected) = target language (error 500)

They are located in `systranio.exceptions`