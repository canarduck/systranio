# Development

Code is hosted on [Gitlab](https://gitlab.com/canarduck/systranio), where a [CI environnement is setup](https://gitlab.com/canarduck/systranio/.gitlab-ci.yml) to automate testing, deployment to PyPi and documentation updates.

## Requirements

```
python3 -m venv .venv/systranio  # build a python3 venv with whatever you want
pip install -r requirements.txt
pip install -r dev-requirements.txt
```

[pip-tools](https://github.com/jazzband/pip-tools) is used to ease requirements' maintenance. 

To add a new package, edit `(dev-)requirements.in` and run `pip-compile (dev-)requirements.in` to build a new `(dev)-requirements.txt`.

To update current virtualenv packages run `pip-sync dev-requirements.txt requirements.txt`

## Documentation

This is (an experiment of a) Documentation Driven Development project, so :

1. Write documentation
2. Write failing tests
3. Write some code to pass previous tests
4. Commit
5. Bump version
6. Repeat

Documentation is written in markdown and built with [MkDocs](http://mkdocs.org/). Online docs are in sync with the latest published version.

## Tests

Try to keep a high level of [coverage](/coverage_html_report) before pushing a PR.

## Roadmap / issues

[Milestones](https://gitlab.com/canarduck/systranio/milestones) and [issues](https://gitlab.com/canarduck/systranio/issues) are published on gitlab.

## Commit messages & gitchangelog

[gitchangelog](https://github.com/vaab/gitchangelog) is a python tool to create a [changelog](https://gitlab.com/canarduck/systranio/blob/master/README.md) from git log history. A git hook (`post-commit`) is used to update the changelog after each commit. To use it :

```
pip install gitchangelog pystache
cp post-commit.sample .git/hooks/post-commit
chmod +x .git/hooks/post-commit
vim .git/hooks/post-commit # Replace PATH_TO_VENV_BIN_ACTIVATE with the absolute path to your venv/bin/activate
```

See [systranio's commit history](https://gitlab.com/canarduck/systranio/commits/master) for some commit message examples and [.gitchangelogrc](https://gitlab.com/canarduck/systranio/blob/master/.gitchangelog.rc) for a detailed documentation. 

## Tags / versions

[bumpversion](https://github.com/peritus/bumpversion) automates the version number update in [setup.py](setup.py) and a git tag creation. 

Usage : `bumpversion major|minor|patch` (respectively increments version number x.y.z) then `git push --tags`. Tags are build and uploaded to PyPi, docs are in sync with the latest version.

## Continuous integration

Tests are launched on a `git push` in [.gitlab-ci.yml](https://gitlab.com/canarduck/systranio/blob/master/.gitlab-ci.yml), an environnemental variable called `SYSTRANIO_KEY` containing your API key is required.

A successful tag build triggers :

* a source build (sdist) uploaded to [PyPi](https://pypi.python.org/pypi/systranio) by gitlab-ci, 2 environnemental variables are required to do this : `PYPI_USER` & `PYPI_PASSWORD`
* an online documentation update